package edu.ntnu.stud.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class TrainRegisterTest {
  private TrainRegister trainRegister;
  private TrainDeparture train1;
  private TrainDeparture train2;

  @BeforeEach
  public void setUp() {
    trainRegister = new TrainRegister();
    trainRegister.updateClock(LocalTime.of(9,0));
    train1 = new TrainDeparture(LocalTime.of(10, 0),"F2",1, "Destination1", LocalTime.of(0,0),1);
    train2 = new TrainDeparture(LocalTime.of(11, 0),"G6",2, "Destination2", LocalTime.of(0,15));
  }

  @org.junit.jupiter.api.Nested
  class PositiveTests {
    @Test
    void testGetTrainDepartureRegister() {
      assertNotNull(trainRegister.getTrainDepartureRegister());
    }

    @Test
    void testGetTime() {
      assertEquals(trainRegister.getTime(), LocalTime.of(9, 0));
    }

    @Test
    void testAddTrain() {
      trainRegister.addTrain(train1);
      assertTrue(trainRegister.getTrainDepartureRegister().containsKey(1));
    }

    @Test
    void testFindTrain() {
      trainRegister.addTrain(train1);
      TrainDeparture foundTrain = trainRegister.findTrain(1);
      assertEquals(train1, foundTrain);
    }

    @Test
    void testGetTrainsToDestination() {
      trainRegister.addTrain(train1);
      List<TrainDeparture> trainsToDestination = trainRegister.getTrainsToDestination("Destination1");
      assertTrue(trainsToDestination.contains(train1));
    }

    @Test
    void testRemoveDepartedTrains() {
      trainRegister.addTrain(train1);
      trainRegister.updateClock(LocalTime.of(10, 30));//Train1 should have departed
      assertFalse(trainRegister.getTrainDepartureRegister().containsKey(1));
    }

    @Test
    void testSortTrainsByDepartureTime() {
      trainRegister.addTrain(train2);//departs 11:15
      trainRegister.addTrain(train1);//departs 10:00
      List<TrainDeparture> sortedTrains = trainRegister.getSortedTrainsByDepartureTime();
      assertEquals(sortedTrains.get(0), train1);
      assertEquals(sortedTrains.get(1), train2);
    }

    @Test
    void testChangeTrackForDeparture() {
      trainRegister.addTrain(train1);
      trainRegister.changeTrackForDeparture(1, 5);
      assertEquals(5, train1.getTrack());
    }

    @Test
    void testChangeDelayForDeparture() {
      LocalTime delay = LocalTime.of(0, 30);
      trainRegister.addTrain(train1);
      trainRegister.changeDelayForDeparture(1, delay);
      assertEquals(delay, train1.getDelay());
    }

    @Test
    void testUpdateClock() {
      LocalTime newTime = LocalTime.of(10, 0);
      trainRegister.updateClock(newTime);
      assertEquals(newTime, trainRegister.getTime());
    }
  }

  @org.junit.jupiter.api.Nested
  class NegativeTests {
    @Test
    void testAddTrainWithExistingTrainNumber() {
      trainRegister.addTrain(train1);//has train number 1
      TrainDeparture newTrain = new TrainDeparture(LocalTime.of(14,0),"F9",1,"Bergen",LocalTime.of(0,0));
      assertThrows(IllegalArgumentException.class, () -> {
        trainRegister.addTrain(newTrain);
      });
    }

    @Test
    void testFindTrainNotInRegister() {
      assertThrows(IllegalArgumentException.class, () -> {
        trainRegister.findTrain(1);
      });
    }

    @Test
    void testGetTrainsToNonExistingDestination() {
      assertThrows(IllegalArgumentException.class, () -> {
        trainRegister.getTrainsToDestination("Oslo");
      });
    }

    @Test
    void testChangeTrackForNonExistingTrainAndInvalidTrack() {
      assertThrows(IllegalArgumentException.class, () -> {
        trainRegister.changeTrackForDeparture(1,2);
      });

      trainRegister.addTrain(train1);
      assertThrows(IllegalArgumentException.class, () -> {
        trainRegister.changeTrackForDeparture(1,0);
      });
    }

    @Test
    void testChangeDelayForNonExistingTrain() {
      LocalTime delay = LocalTime.of(0,1);
      assertThrows(IllegalArgumentException.class, () -> {
        trainRegister.changeDelayForDeparture(1,delay);
      });
    }

    @Test
    void testUpdateClockInvalidTime() {
      LocalTime invalidTime = LocalTime.of(8, 0);
      assertThrows(IllegalArgumentException.class, () -> {
        trainRegister.updateClock(invalidTime);
      });
    }
    @Test
    void testToStringEmptyRegister() {
      assertEquals("No trains in register", trainRegister.toString());
    }
  }
}