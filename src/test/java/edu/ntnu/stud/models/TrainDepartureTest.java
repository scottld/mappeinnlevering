package edu.ntnu.stud.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TrainDepartureTest {
  private TrainDeparture train1;
  private TrainDeparture train2;

  @BeforeEach
  public void setUp() {
    //train 1 has a given track
    train1 = new TrainDeparture(LocalTime.of(10, 0), "F2", 1, "Destination1", LocalTime.of(0, 0), 1);
    //train 2 has no given track in constructor
    train2 = new TrainDeparture(LocalTime.of(11, 0), "G6", 2, "Destination2", LocalTime.of(0, 0));
  }

  @org.junit.jupiter.api.Nested
  class PositiveTestsConstructorWithTrack {
    @Test
    void testGetDepartureTime() {
      assertEquals(LocalTime.of(10, 0), train1.getDepartureTime());
    }

    @Test
    void testGetLine() {
      assertEquals("F2", train1.getLine());
    }

    @Test
    void testGetTrainNumber() {
      assertEquals(1, train1.getTrainNumber());
    }

    @Test
    void testGetDestination() {
      assertEquals("Destination1", train1.getDestination());
    }

    @Test
    void testGetDelay() {
      assertEquals(LocalTime.of(0, 0), train1.getDelay());
    }

    @Test
    void testGetTrack() {
      assertEquals(1, train1.getTrack());
    }
  }

  @org.junit.jupiter.api.Nested
  class PositiveTestsConstructorWithoutTrack {
    @Test
    void testGetDepartureTime() {
      assertEquals(LocalTime.of(11, 0), train2.getDepartureTime());
    }

    @Test
    void testGetLine() {
      assertEquals("G6", train2.getLine());
    }

    @Test
    void testGetTrainNumber() {
      assertEquals(2, train2.getTrainNumber());
    }

    @Test
    void testGetDestination() {
      assertEquals("Destination2", train2.getDestination());
    }

    @Test
    void testGetDelay() {
      assertEquals(LocalTime.of(0, 0), train2.getDelay());
    }

    @Test
    void testGetTrack() {
      assertEquals(-1, train2.getTrack());
    }
  }

  @org.junit.jupiter.api.Nested
  class NegativeTestsConstructorWithTrack {
    //LocalTime can also throw exceptions
    LocalTime departureTime = LocalTime.of(12, 0);
    LocalTime delay = LocalTime.of(0, 0);

    @Test
    void testInvalidTrainNumber() {
      assertThrows(IllegalArgumentException.class, () ->
          new TrainDeparture(departureTime, "Line1", 0, "Destination", delay, 1));
    }

    @Test
    void testInvalidTrack() {
      assertThrows(IllegalArgumentException.class, () ->
          new TrainDeparture(departureTime, "Line1", 1, "Destination", delay, 0));
    }

    @Test
    void testInvalidDestination() {
      assertThrows(IllegalArgumentException.class, () ->
          new TrainDeparture(departureTime, "Line1", 1, "", delay, 1));
    }

    @Test
    void testInvalidLine() {
      assertThrows(IllegalArgumentException.class, () ->
          new TrainDeparture(departureTime, "", 1, "Destination", delay, 1));
    }
  }

  @org.junit.jupiter.api.Nested
  class NegativeTestsConstructorWithoutTrack {
    //LocalTime can also throw exceptions
    LocalTime departureTime = LocalTime.of(12, 0);
    LocalTime delay = LocalTime.of(0, 0);

    @Test
    void testInvalidTrainNumber() {
      assertThrows(IllegalArgumentException.class, () ->
          new TrainDeparture(departureTime, "Line1", 0, "Destination", delay));
    }

    @Test
    void testInvalidDestination() {
      assertThrows(IllegalArgumentException.class, () ->
          new TrainDeparture(departureTime, "Line1", 1, "", delay));
    }

    @Test
    void testInvalidLine() {
      assertThrows(IllegalArgumentException.class, () ->
          new TrainDeparture(departureTime, "", 1, "Destination", delay));
    }
  }


  @org.junit.jupiter.api.Nested
  class PositiveTestsTrainDepartureMethods {
    @Test
    void testSetTrack() {
      train2.setTrack(2);
      assertEquals(2, train2.getTrack());
    }

    @Test
    void testSetDelay() {
      LocalTime newDelay = LocalTime.of(0, 15);
      train2.setDelay(newDelay);
      assertEquals(newDelay, train2.getDelay());
    }

    @Test
    void testGetTotalTime() {
      train2.setDelay(LocalTime.of(0, 15));
      assertEquals(LocalTime.of(11, 15), train2.getTotalTime());
    }

    @Test
    void testCompareTo() {
      assertEquals(-1, train1.compareTo(train2));
      assertEquals(1, train2.compareTo(train1));
      //New train with the same departure time as train1
      TrainDeparture train3 = new TrainDeparture(LocalTime.of(10, 0), "F2", 1, "Destination1", LocalTime.of(0, 0), 1);
      assertEquals(0, train1.compareTo(train3));
    }
  }

  @org.junit.jupiter.api.Nested
  class NegativeTestsTrainDepartureMethods {
    @Test
    void testSetInvalidTrack() {
      assertThrows(IllegalArgumentException.class, () -> train2.setTrack(0));
    }
  }
}