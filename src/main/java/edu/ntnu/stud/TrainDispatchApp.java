package edu.ntnu.stud;

import edu.ntnu.stud.userinterface.UserInterface;

/**
 * <b>The main class for the train dispatch application.</b>
 *
 * @author Scott du Plessis
 * @version 3.4
 * @since 20.11.2023
 */
public class TrainDispatchApp {
  private static final UserInterface userInterface = new UserInterface();

  /**
   * Main method of the application.
   *
   * @param args arguments for main method
   */
  public static void main(String[] args) {
    userInterface.init();
    userInterface.start();
  }
}
