package edu.ntnu.stud.utils;

import java.time.DateTimeException;
import java.time.LocalTime;
import java.util.Scanner;

/**
 * <b>A utility class that contains methods that are used in the other classes of the program.</b>
 *
 * @author Scott du Plessis
 * @version 3.4
 * @since 20.11.2023
 */
public class TrainUtils {
  private static final Scanner scanner = new Scanner(System.in);

  private TrainUtils() {
    //Private constructor to prevent instantiation
  }

  /**
   * Makes the header and separation line for any table that is to show one or more
   *        <b>TrainDeparture</b> object.
   *
   * @return the header and separation line for tables that show departures
   */
  public static String getTableHeader() {
    return String.format("| %-15s | %-15s | %-15s | %-15s | %-15s | %-15s |%n",
        "Time", "Line", "Train number", "Destination", "Delay", "Track")
        + "------------------------------------------------------------------------------------"
        + "-------------------------\n";
  }

  /**
   * Takes in a String from user that the method will try to parse into a <b>LocalTime</b> object.
   *
   * @return A <b>LocalTime</b> object that has been parsed from a String that the user has given
   */
  public static LocalTime parseLocalTimeFromInput() {
    LocalTime parsedTime = null;

    boolean parsed = false;

    while (!parsed) {
      System.out.println("Enter the time or amount of time in the format hh:mm");
      String timeString = scanner.nextLine();
      try {
        parsedTime = LocalTime.parse(timeString);
        parsed = true;
      } catch (DateTimeException dte) {
        System.out.println("The time \"" + timeString + "\" couldn't be converted to LocalTime,"
            + " make sure its in the format hh:mm");
      }
    }
    return parsedTime;
  }

  /**
   * Takes in a String from the terminal from the user. The method then tries to convert
   * the string to an integer and also checks if the integer is positive.
   *
   * @return The first positive integer the user enters.
   */
  public static int parsePositiveIntegerFromInput() {
    boolean converted = false;

    int stringConvertedToInt = 0; //have to give it an initial value

    while (!converted) {
      System.out.println("Enter a positive integer");
      String integerInStringFormat = scanner.nextLine();
      try {
        stringConvertedToInt = Integer.parseInt(integerInStringFormat);
        checkIfIntegerIsPositive(stringConvertedToInt);
        converted = true;
      } catch (Exception e) { //multiple types of exceptions are being thrown
        System.out.println("Couldn't convert " + integerInStringFormat + " to a positive integer");
      }
    }
    return stringConvertedToInt;
  }

  /**
   * Takes in a string from the user in the terminal and uses the checkIfStringIsEmpty to check
   * if the string is valid to use.
   *
   * @return the first valid String the user enters
   */
  public static String getStringInput() {
    boolean finished = false;
    String validString = null;

    while (!finished) {
      System.out.println("Do not enter an empty or blank string:");
      String stringInput = scanner.nextLine();
      try {
        validString = checkIfStringIsEmpty(stringInput);
        finished = true;
      } catch (IllegalArgumentException iae) {
        System.out.println(iae.getMessage());
      }
    }
    return validString.trim(); //removes extra spaces before and after the text
  }

  /**
   * Checks if an integer is positive.
   *
   * @param num the integer that is being checked if it is positive
   * @return the same value that is the num parameter
   * @throws IllegalArgumentException if the parameter is not positive
   */
  public static int checkIfIntegerIsPositive(int num) throws IllegalArgumentException {
    if (num < 1) {
      throw new IllegalArgumentException("The value should be a positive integer, the value "
          + num +  "is invalid");
    } else {
      return num;
    }
  }

  /**
   * Checks if a string is blank or empty. Blank meaning that the string consists of only spaces
   *        and empty meaning a string with length 0.
   *
   * @param string The string that is checked for being empty or blank
   * @return the same string that is given as the string parameter
   * @throws IllegalArgumentException if the string is blank or empty
   */
  public static String checkIfStringIsEmpty(String string) throws IllegalArgumentException {
    if (string.isEmpty() || string.isBlank()) {
      throw new IllegalArgumentException("Cant add an empty or blank string, please try again");
    } else {
      return string;
    }
  }
}
