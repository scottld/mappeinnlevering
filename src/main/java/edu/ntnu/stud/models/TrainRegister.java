package edu.ntnu.stud.models;

import edu.ntnu.stud.utils.TrainUtils;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * A register for all <b>TrainDeparture</b> objects that are made.
 *
 * @author Scott du Plessis
 * @version 3.4
 * @since 20.11.2023
 */

public class TrainRegister {
  // Every value in the HashMap is a TrainDeparture object
  // Every key in the map is the train number field of its corresponding value
  private final Map<Integer, TrainDeparture> trainDepartureRegister = new HashMap<>();
  private LocalTime clock;

  public LocalTime getTime() {
    return this.clock;
  }

  public Map<Integer, TrainDeparture> getTrainDepartureRegister() {
    return trainDepartureRegister;
  }

  /**
   * Method that adds a train-departure to the register.
   *
   * @param train the TrainDeparture that is being added
   * @throws IllegalArgumentException if the departure that is being added has the same train
   *                                  number as another train that already exists
   */
  public void addTrain(TrainDeparture train) throws IllegalArgumentException {
    //Checks if there is a train in the register with the same train number
    if (trainDepartureRegister.containsKey(train.getTrainNumber())) {
      throw new IllegalArgumentException("A Train Departure with the same "
          + "Train Number already exists in register");
    } else {
      // Adding the departure to the HashMap
      trainDepartureRegister.put(train.getTrainNumber(), train);
    }
  }

  /**
   * Takes a train number as param and returns the <b>TrainDeparture</b> object in the
   * register with matching train number.
   *
   * @param trainNumber the train number for the train that is being found in the register
   * @return A <b>TrainDeparture</b> obj with the same train number which was given as a parameter
   * @throws IllegalArgumentException if a train with train number from param does not exist
   */
  public TrainDeparture findTrain(int trainNumber) throws IllegalArgumentException {
    // Checks if there exists a train in the register with the given train number
    if (trainDepartureRegister.containsKey(trainNumber)) {
      return trainDepartureRegister.get(trainNumber);
    } else {
      throw new IllegalArgumentException("Train Departure does not exist in register");
    }
  }

  /**
   * Searches for all <b>TrainDeparture</b> objects to a specific destination.
   *
   * <p>
   *   Makes a stream of the values of the HashMap and filters them by destination.
   *   The filtered trains are then collected to a list.
   * </p>
   *
   * @param destination the trains that go to this destination are being found
   * @return A list that contains the trains that are going to the specific destination
   * @throws IllegalArgumentException if there are no trains to the given destination
   */
  public List<TrainDeparture> getTrainsToDestination(String destination)
      throws IllegalArgumentException {
    // A stream is made of every object in the List
    // Then checks if the destination matches with the given destination in the parameter
    List<TrainDeparture> trainsToDestination = trainDepartureRegister.values()
        .stream()
        .filter(train -> train.getDestination().equalsIgnoreCase(destination))
        .toList(); // Terminating the stream by collecting all the relevant objects to a list

    //Checking whether there are any trains to the given destination
    if (trainsToDestination.isEmpty()) {
      throw new IllegalArgumentException("No trains to destination");
    } else {
      return trainsToDestination;
    }
  }

  /**
   * Removes all <b>TrainDeparture</b> objects that have departed from the register.
   *
   * <p>
   *   Uses an iterator that iterates through the HashMap and removes the trains that have
   *   departed from the register. The iterator is used to avoid a ConcurrentModificationException
   *   that would occur if the HashMap was iterated through with any other loop.
   * </p>
   */
  public void removeDepartedTrains() {
    //Creating the HashMap iterator
    Iterator<Map.Entry<Integer, TrainDeparture>> trainRegisterIterator =
        trainDepartureRegister.entrySet().iterator();

    while (trainRegisterIterator.hasNext()) {
      Map.Entry<Integer, TrainDeparture> entry = trainRegisterIterator.next();
      TrainDeparture train = entry.getValue();
      //Checking if every TrainDeparture total time is before or equal to current time
      if (train.getTotalTime().minusMinutes(1).isBefore(clock)) {
        //Remove the key and value pair if true
        trainRegisterIterator.remove();
      }
    }
  }

  /**
   * Sorts all the trains by their departure time.
   * <p>
   *   Makes a stream of the values of the HashMap and sorts them by departure time
   *   using the compareTo() method of the <b>TrainDeparture</b> class.
   *   The sorted trains are then collected to a list.
   * </p>
   *
   * @return a list of all the trains sorted by departure time
   */
  public List<TrainDeparture> getSortedTrainsByDepartureTime() {
    return trainDepartureRegister.values()
        .stream()
        .sorted(TrainDeparture::compareTo)
        .toList();
  }

  /**
   * Changes the track of a <b>TrainDeparture</b> object that has a given train number.
   *
   * <p>
   *   To find the correct <b>TrainDeparture</b> object the findTrain() method of this
   *   class is used, and then the setTrack() method of the <b>TrainDeparture</b> object
   *   is used to set or reset the track
   * </p>
   *
   * @param trainNum the train number for the element that the track is being changed for
   * @param trackNum the new track number for a train
   * @throws IllegalArgumentException if a train with the given train number does not exist or
   *     the track number is invalid
   */

  public void changeTrackForDeparture(int trainNum, int trackNum) throws IllegalArgumentException {
    this.findTrain(trainNum).setTrack(trackNum);
  }

  /**
   * Changes the delay of a <b>TrainDeparture</b> object that has a given train number.
   *
   * <p>
   *   To find the correct <b>TrainDeparture</b> object the findTrain() method of this
   *   class is used, and then the setDelay() method of the <b>TrainDeparture</b> object
   *   is used to change or set the delay
   * </p>
   *
   * @param trainNum the train number for the TrainDeparture object of which the delay is
   *     being changed
   * @param delay the new delay for a given train
   * @throws IllegalArgumentException if there is no train with the given train number
   */
  public void changeDelayForDeparture(int trainNum, LocalTime delay)
      throws IllegalArgumentException {
    this.findTrain(trainNum).setDelay(delay);
  }

  /**
   * Updates the clock of the train register.
   *
   * <p>
   *   The clock is only changed if the clock is null or if the new time being set
   *   is after the current time. The clock is null before the time has been set by the user
   *   hence why the time can be changed if the clock is null.
   * </p>
   *
   * @param newTime the new time that the user is setting
   * @throws IllegalArgumentException if newTime param is before the current time
   */
  public void updateClock(LocalTime newTime) throws IllegalArgumentException {
    if (clock != null && clock.isAfter(newTime)) {
      throw new IllegalArgumentException("New time is before current time, couldn't change time");
    } else {
      clock = newTime;
      removeDepartedTrains();
    }
  }

  /**
   * Builds a table with all <b>TrainDeparture</b> objects that haven't departed yet by using
   *        the <b>StringBuilder</b> class.
   * <p>
   *   The method removes all the departed trains before checking if the register is empty
   *   or making the table
   * </p>
   *
   * @return a string containing all the departures in the register sorted by departure time
   *        or a string displaying that the register is empty
   */
  @Override
  public String toString() {
    removeDepartedTrains();
    List<TrainDeparture> sortedDepartures = getSortedTrainsByDepartureTime();

    if (sortedDepartures.isEmpty()) {
      return "No trains in register";
    } else {
      StringBuilder stringBuilder = new StringBuilder("Next departures from " + clock + ": \n");
      stringBuilder.append(TrainUtils.getTableHeader());
      sortedDepartures.forEach(
          departure -> stringBuilder.append(departure.toString()).append("\n"));

      return stringBuilder.toString();
    }
  }
}