package edu.ntnu.stud.models;

import edu.ntnu.stud.utils.TrainUtils;
import java.time.LocalTime;

/**
 * <b>The entity class for a single train departure.</b>
 *
 * @author Scott du Plessis
 * @version 3.4
 * @since 21.11.2023
 */
public class TrainDeparture implements Comparable<TrainDeparture> {
  private LocalTime delay;
  private final int trainNumber;
  private int track;
  private final LocalTime departureTime;
  private final String destination;
  private final String line;

  /**
   * Constructor for TrainDeparture class when a track is given.
   *
   * @param departureTime the time of departure
   * @param line          the train line
   * @param trainNumber   the train number
   * @param destination   the destination of the train
   * @param track         the track on which the train will depart from
   * @param delay         the delay of the train
   * @throws IllegalArgumentException if track or trainNumber is outside valid range, and if
   *                                 line or destination is empty or blank.
   */
  public TrainDeparture(LocalTime departureTime, String line, int trainNumber, String destination,
                        LocalTime delay, int track) throws IllegalArgumentException {
    this.departureTime = departureTime;
    this.line = TrainUtils.checkIfStringIsEmpty(line);
    this.destination = TrainUtils.checkIfStringIsEmpty(destination);
    this.delay = delay;
    setTrack(track);
    this.trainNumber = TrainUtils.checkIfIntegerIsPositive(trainNumber);
  }

  /**
   * Constructor for TrainDeparture class when there is no track given.
   *
   * @param departureTime the time of departure
   * @param line          the train line
   * @param trainNumber   the train number
   * @param destination   the destination of the train
   * @param delay         the delay of the train
   * @throws IllegalArgumentException if trainNumber is outside valid range, and if line or
   *                                 destination is empty or blank.
   */
  public TrainDeparture(LocalTime departureTime, String line, int trainNumber, String destination,
                        LocalTime delay) throws IllegalArgumentException {
    this.departureTime = departureTime;
    this.line = TrainUtils.checkIfStringIsEmpty(line);
    this.trainNumber = TrainUtils.checkIfIntegerIsPositive(trainNumber);
    this.destination = TrainUtils.checkIfStringIsEmpty(destination);
    this.delay = delay;
    this.track = -1;
  }

  public LocalTime getDepartureTime() {
    return departureTime;
  }

  public String getLine() {
    return line;
  }

  public String getDestination() {
    return destination;
  }

  public int getTrack() {
    return track;
  }

  public LocalTime getDelay() {
    return delay;
  }

  public int getTrainNumber() {
    return trainNumber;
  }

  /**
   * Calculates the time a <b>TrainDeparture</b> object will depart with the delay taken
   *       into account.
   *
   * @return A <b>LocalTime</b> object that represents a <b>TrainDeparture</b> objects
   *        departure time plus delay
   */
  public LocalTime getTotalTime() {
    return departureTime.plusHours(delay.getHour()).plusMinutes(delay.getMinute());
  }

  /**
   * Sets or resets the track of a <b>TrainDeparture</b> object.
   *
   * @param track the track on which the train will depart from
   *              and the track that will be set for this object
   * @throws IllegalArgumentException if the track is not a positive integer
   */
  public void setTrack(int track) throws IllegalArgumentException {
    this.track = TrainUtils.checkIfIntegerIsPositive(track);
  }

  /**
   * Sets or resets the delay of a train departure.
   *
   * @param delay A <b>LocalTime</b> object that represents the delay of the train
   */
  public void setDelay(LocalTime delay) {
    this.delay = delay;
  }

  /**
   * Builds the string that represents the departure, and checks which variables
   *        to include in the return statement.
   *
   * @return A formatted String that shows information about the departure
   */
  @Override
  public String toString() {

    String formatForTable = "| %-15s | %-15s | %-15s | %-15s | %-15s | %-15s |";

    LocalTime noDelay = LocalTime.of(0, 0);

    if (delay.equals(noDelay) && track != -1) {
      //departure has no delay and has a track
      return String.format(
          formatForTable, departureTime, line, trainNumber, destination, "", track);

    } else if (!delay.equals(noDelay) && track != -1) {
      //departure has delay and has a track
      return String.format(formatForTable, departureTime, line, trainNumber, destination,
          delay, track);

    } else if (delay.equals(noDelay) && track == -1) {
      //departure has no delay and no track
      return String.format(formatForTable, departureTime, line, trainNumber, destination, "", "");

    } else {
      //departure has to have delay and no track for the last case
      return String.format(
          formatForTable, departureTime, line, trainNumber, destination, delay, "");
    }
  }

  /**
   * Method that compares two <b>TrainDeparture</b> objects by their departure time,
   *        is used when <b>TrainDeparture</b> objects are being sorted.
   *
   * @param otherDeparture the object that this object is being compared against.
   *
   * @return returns a negative integer if the TrainDeparture passed as param departs after this,
   *     returns 0 if this and otherDeparture param depart at the same time, and it returns a
   *     positive integer if this TrainDeparture departs after the departure passed as a parameter.
   */
  @Override
  public int compareTo(TrainDeparture otherDeparture) {
    return this.getDepartureTime().compareTo(otherDeparture.getDepartureTime());
  }
}