package edu.ntnu.stud.userinterface;

import edu.ntnu.stud.models.TrainDeparture;
import edu.ntnu.stud.models.TrainRegister;
import edu.ntnu.stud.utils.TrainUtils;
import java.time.LocalTime;
import java.util.List;
import java.util.Scanner;

/**
 * <b>The user interface of the train dispatch application.</b>
 *
 * <p>
 *   Handles all interaction with the user through the terminal,
 *   also validates input using try and catch statements.
 * </p>
 *
 * @author Scott du Plessis
 * @version 3.4
 * @since 20.11.2023
 */

public class UserInterface {
  private static final Scanner scanner = new Scanner(System.in);
  private static TrainRegister trainRegister;

  /**
   * Prints the main menu of the application and shows the user
   * which actions they can perform.
   */
  private void printMenu() {
    System.out.println("Current time: " + trainRegister.getTime());
    System.out.println("Please choose what action you want to do by "
        + "typing the number that represents the action");
    System.out.println("1. Add a train to the system");
    System.out.println("2. Show all departures");
    System.out.println("3. Add or change track to an existing departure");
    System.out.println("4. Add or change delay to a departure");
    System.out.println("5. Search for and show a departure based on its train number");
    System.out.println("6. Search for and show departure(s) based on destination");
    System.out.println("7. Update clock");
    System.out.println("0. End the application");
  }

  /**
   * The method that is run to initialize the app.
   *
   * <p>
   *   The necessary actions that are done so that the application can run is to
   *   initialize a <b>TrainRegister</b> object that can contain <b>TrainDeparture</b> objects.
   *   Also example <b>TrainDeparture</b> objects are added to the register,
   *   and the clock gets initialized.
   * </p>
   */
  public void init() {

    trainRegister = new TrainRegister();

    TrainDeparture dep1 = new TrainDeparture(
        LocalTime.of(21, 2), "D5", 1, "Drammen", LocalTime.of(0, 10));
    TrainDeparture dep2 = new TrainDeparture(
        LocalTime.of(12, 0), "f5", 2, "Oslo", LocalTime.of(0, 0), 4);
    TrainDeparture dep3 = new TrainDeparture(
        LocalTime.of(10, 30), "g9", 4, "Skien", LocalTime.of(0, 40), 8);
    TrainDeparture dep4 = new TrainDeparture(
        LocalTime.of(15, 59), "t5", 12, "Oslo", LocalTime.of(0, 2));
    TrainDeparture dep5 = new TrainDeparture(
        LocalTime.of(23, 0), "b7", 66, "Drammen", LocalTime.of(0, 0));


    trainRegister.addTrain(dep1);
    trainRegister.addTrain(dep2);
    trainRegister.addTrain(dep3);
    trainRegister.addTrain(dep4);
    trainRegister.addTrain(dep5);




    System.out.println("Welcome to the train dispatch application!");

    verifyUpdateClock();

  }

  /**
   * The method that is run to start the application.
   *
   * <p>
   *   The main loop of the program is run here, as well as the correct methods are run
   *   in the correct circumstances. The correct methods are run by using a switch statement
   *   to check which action the user wants to execute.
   * </p>
   */
  public void start() {

    boolean finished = false;

    while (!finished) {
      printMenu();
      String chosenAction = scanner.nextLine();
      switch (chosenAction) {
        case "1":
          //add a new train to the system
          verifyAddTrain();
          System.out.println(trainRegister);
          break;
        case "2":
          //show all departures
          System.out.println(trainRegister);
          break;
        case "3":
          //add or change track to an existing departure
          verifyChangeTrack();
          System.out.println(trainRegister);
          break;
        case "4":
          //Add delay to a departure
          verifyChangeDelay();
          System.out.println(trainRegister);
          break;
        case "5":
          //Search for and show a departure based on its train number
          verifyFindTrainByTrainNum();
          break;
        case "6":
          //Search for and show departure(s) based on destination
          verifyFindDeparturesByDestination();
          break;
        case "7":
          //update the clock
          verifyUpdateClock();
          System.out.println(trainRegister);
          break;
        case "0":
          //end application
          System.out.println("Exiting application...");
          finished = true;
          break;
        default:
          System.out.println("You didnt enter a number between 0 and 7, try again");
      }
    }
  }

  /**
   * Method that takes in the train number that represents the <b>TrainDeparture</b> object the
   * user wants to find. Then verifies that the train number is a positive integer and lastly
   * verifies that a train with that train number exist.
   */
  private static void verifyFindTrainByTrainNum() {
    if (trainRegister.getTrainDepartureRegister().isEmpty()) {
      System.out.println("The register does not contain any trains to check for");
      return;
    }

    System.out.println("Type the TRAIN NUMBER for the departure you want to show");
    int trainNumber = TrainUtils.parsePositiveIntegerFromInput();
    
    TrainDeparture departureWithTrainNum = null;

    try {
      departureWithTrainNum = trainRegister.findTrain(trainNumber);
    } catch (IllegalArgumentException iae) {
      System.out.println(iae.getMessage());
    }

    if (departureWithTrainNum != null) {
      System.out.print(TrainUtils.getTableHeader());
      System.out.println(departureWithTrainNum);
    }
  }

  /**
   * Takes in the destination that the user wants to find the <b>TrainDeparture</b> objects to
   * and prints them, alternatively it prints that there are no trains to the chosen destination.
   */
  private static void verifyFindDeparturesByDestination() {
    if (trainRegister.getTrainDepartureRegister().isEmpty()) {
      System.out.println("The register does not contain any trains to check for");
      return;
    }

    List<TrainDeparture> departuresToDestination = null;

    System.out.println("Enter the DESTINATION you want to find the trains to");
    String destination = TrainUtils.getStringInput();

    try {
      departuresToDestination = trainRegister.getTrainsToDestination(destination);
    } catch (IllegalArgumentException iae) {
      System.out.println(iae.getMessage());
    }


    if (departuresToDestination != null && !departuresToDestination.isEmpty()) {
      System.out.println("Trains to " + destination + ":");
      System.out.print(TrainUtils.getTableHeader());
      departuresToDestination.forEach(System.out::println);
    }
  }

  /**
   * Takes the input for a track change of a <b>TrainDeparture</b> object from the user.
   *
   * <p>
   *   Both the train number and track that the user wants to change to is taken in through the
   *   terminal and validated with try and catch statements.
   * </p>
   */
  private static void verifyChangeTrack() {
    if (trainRegister.getTrainDepartureRegister().isEmpty()) {
      System.out.println("The register does not contain any trains to check for");
      return;
    }

    boolean finished = false;

    while (!finished) {
      System.out.println("enter the TRAIN NUMBER for the train you want ot add/change track for");
      int trainNumber = TrainUtils.parsePositiveIntegerFromInput();
      System.out.println("Enter the TRACK you want to add / change to");
      int trackNum = TrainUtils.parsePositiveIntegerFromInput();

      try {
        trainRegister.changeTrackForDeparture(trainNumber, trackNum);
        System.out.println("Track changed");
        finished = true;
      } catch (IllegalArgumentException iae) {
        System.out.println(iae.getMessage());
      }
    }
  }

  /**
   * Takes the input for a delay change of a <b>TrainDeparture</b> object from the user.
   *
   * <p>
   *   Both the train number and delay that the user wants to change to is taken in through the
   *   terminal and validated with try and catch statements.
   * </p>
   */
  private static void verifyChangeDelay() {
    if (trainRegister.getTrainDepartureRegister().isEmpty()) {
      System.out.println("The register does not contain any trains to check for");
      return;
    }

    boolean finished = false;

    while (!finished) {
      System.out.println("enter the TRAIN NUMBER for the train you want to change the delay for");
      int trainNumber = TrainUtils.parsePositiveIntegerFromInput();
      System.out.println("Enter the amount of DELAY you want to add");
      LocalTime newDelay = TrainUtils.parseLocalTimeFromInput();

      try {
        trainRegister.changeDelayForDeparture(trainNumber, newDelay);
        System.out.println("Delay changed");
        finished = true;
      } catch (IllegalArgumentException iae) {
        System.out.println(iae.getMessage());
      }
    }
  }


  /**
   * Takes in a new time to change the clock to.
   *
   * <p>
   *   Try and catch statements are used to make sure that the time change can be applied
   *   and the new time that the user puts in is validated in the
   *   <b>parseLocalTimeFromInput()</b> method
   * </p>
   */
  private static void verifyUpdateClock() {
    boolean clockUpdated = false;

    while (!clockUpdated) {
      System.out.println("Enter the new time");
      LocalTime newTime = TrainUtils.parseLocalTimeFromInput();
      try {
        trainRegister.updateClock(newTime);
        clockUpdated = true;
        System.out.println("Clock updated");
      } catch (IllegalArgumentException iae) {
        System.out.println(iae.getMessage());
      }
    }
  }

  /**
   * Takes inn all input from the terminal that needs to be taken for a new instance of the
   * <b>TrainDeparture</b> class to be made.
   */
  private static void verifyAddTrain() {
    boolean finished = false;

    while (!finished) {
      TrainDeparture newDeparture;
      System.out.println("Enter the DEPARTURE TIME of the train departure you want to add");
      LocalTime departureTime = TrainUtils.parseLocalTimeFromInput();
      System.out.println("Enter the DELAY of the train");
      LocalTime delay = TrainUtils.parseLocalTimeFromInput();
      System.out.println("Enter the DESTINATION of the train");
      String destination = TrainUtils.getStringInput();
      System.out.println("Enter the LINE the train is driving");
      String line = TrainUtils.getStringInput();
      System.out.println("Enter the TRAIN NUMBER of the train");
      int trainNumber = TrainUtils.parsePositiveIntegerFromInput();
      System.out.println("Do you want to add a TRACK to the train? Type \"y\" or \"Y\" for yes,"
          + " anything else for no");
      String addTrackYesOrNo = scanner.nextLine();

      if (addTrackYesOrNo.equalsIgnoreCase("y")) {
        //add a train to the register that has a given track
        System.out.println("Enter the TRACK for the departure");
        int track = TrainUtils.parsePositiveIntegerFromInput();
        try {
          //add a train to the register that has a given track
          newDeparture = new TrainDeparture(
              departureTime, line, trainNumber, destination, delay, track);
          trainRegister.addTrain(newDeparture);
          System.out.println("Train added");
          finished = true;
        } catch (IllegalArgumentException iae) {
          System.out.println(iae.getMessage());
        }
      } else {
        //add a train to the register that doesn't have a given track
        try {
          newDeparture = new TrainDeparture(departureTime, line, trainNumber, destination, delay);
          trainRegister.addTrain(newDeparture);
          System.out.println("Train added");
          finished = true;
        } catch (IllegalArgumentException iae) {
          System.out.println(iae.getMessage());
        }
      }
    }
  }
}