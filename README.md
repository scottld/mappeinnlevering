# Portfolio project IDATT1003 - 2023

STUDENT NAME = Scott Langum du Plessis  
CANDIDATE NUMBER = 10015

## Project description

This project is the final product of an assignment of the class IDATT1003 at NTNU Trondheim. The assignment is based on making a system for displaying and editing train departures for a single train station. 

## Project structure

The project is a Maven project. To find the sourcefiles go to src folder in the project folder, then follow main->java->edu->ntnu->stud and the sourcefiles are located inside the stud folder. To find the JUnit tests from the project folder follow src->test->java->edu->ntnu->stud and the tests are located in the stud folder. Both the JUnit tests and Sourcefiles are in package edu.ntnu.stud. (TODO: Describe the structure of your project here. How have you used packages in your structure. Where are all sourcefiles stored. Where are all JUnit-test classes stored. etc.)

## Link to repository

Link to central git repository on GitLab [here](https://gitlab.stud.idi.ntnu.no/scottld/mappeinnlevering)

## How to run the project

The main class of the project is the TrainDispatchApp class, inwhich the main method of the program lies, which is the one to run for the program to run. The output of the program is through the terminal, that is where the menu will be repsresented and all messages to the user. The input from the user is also supposed to be written directly in the termianl when the program says to do so. The expected behaviour is that the program will start by asking the user to to enter a time so that the program can start working. then the menu will appear and the user can choose what to do next. There are 5 instances of the TrainDeparture class added to the system before the user does anything. These can be removed if the user wants to by either deleting or commenting them out in the code. They are found in the init() method of the UserInterface class.

## How to run the tests

Open the project in for example IntelliJ and find the folder where the test classes lie. Just next to the line numbers there is a button that can be pressed to run the tests, when they are run the IDE will show which tests passed and whoch did not pass.

